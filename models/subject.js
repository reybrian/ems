'use strict';
module.exports = (sequelize, DataTypes) => {
  const Subject = sequelize.define('Subject', {
    subjectCode: DataTypes.STRING,
    isGPA: DataTypes.BOOLEAN,
    subjectDescription: DataTypes.STRING,
    totalWeeks: DataTypes.INTEGER,
    totalHours: DataTypes.INTEGER,
    lectureUnits: DataTypes.INTEGER,
    labUnits: DataTypes.INTEGER,
    totalUnits: DataTypes.INTEGER,
    labFee: DataTypes.INTEGER,
    facultyCredit: DataTypes.INTEGER
  }, {});
  Subject.associate = function(models) {
    // associations can be defined here
    Subject.hasMany(models.SubjectOffering, {
      foreignKey: 'subjectCode',
      targetKey: 'subjectCode',
      as: 'subject-offerings'
    });

  };
  return Subject;
};