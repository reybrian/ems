'use strict';
module.exports = (sequelize, DataTypes) => {
  const Account = sequelize.define('Account', {
    schoolEmail: DataTypes.STRING,
    password: DataTypes.STRING,
    idNumber: DataTypes.STRING,
    roleID: DataTypes.INTEGER,
    isActive: DataTypes.BOOLEAN
  }, {});
  Account.associate = function(models) {
    // associations can be defined here
    Account.belongsTo(models.Role, {
      foreignKey: 'roleID'
    });

    Account.belongsTo(models.Clerk, {
      foreignKey: 'idNumber',
      as: 'clerkprofile'
    });

    Account.belongsTo(models.Student, {
      foreignKey: 'idNumber',
      as: 'studentprofile'
    });

    Account.belongsTo(models.Faculty, {
      foreignKey: 'idNumber',
      as: 'facultyprofile'
    });

  };
  return Account;
};