'use strict';
module.exports = (sequelize, DataTypes) => {
  const Department = sequelize.define('Department', {
    departmentCode: DataTypes.STRING,
    departmentDescription: DataTypes.STRING,
    collegeCode: DataTypes.STRING,
    active: DataTypes.BOOLEAN
  }, {});
  Department.associate = function(models) {
    // associations can be defined here
    
    Department.belongsTo(models.DepartmentChairman, {
      foreignKey: 'departmentCode',
      targetKey: 'departmentCode',
      as: 'departmentchairman'
    })

    Department.belongsTo(models.College,{
      foreignKey: 'collegeCode',
      targetKey: 'collegeCode',
      as: 'college'
    });

    Department.hasMany(models.Block,{
      foreignKey: 'forDepartment',
      targetKey: 'departmentCode',
      as: 'blocks'
    });

    Department.hasMany(models.Program,{
      foreignKey: 'forDepartment',
      targetKey: 'departmentCode',
      as: 'departments'
    });

    Department.hasMany(models.SubjectOffering,{
      foreignKey: 'deliveringDepartment',
      targetKey: 'departmentCode',
      as: 'subjects-delivered'
    });

  };
  return Department;
};