'use strict';
module.exports = (sequelize, DataTypes) => {
  const Enlistment = sequelize.define('Enlistment', {
    schoolYear: DataTypes.INTEGER,
    studentId: DataTypes.STRING,
    yearLevel: DataTypes.INTEGER,
    programCode: DataTypes.STRING,
    majorCode: DataTypes.STRING,
    graduating: DataTypes.BOOLEAN,
  }, {
    defaultScope: {
      attributes: { exclude: ['subjectOffering'] }
    }
  });
  Enlistment.associate = function(models) {
    // associations can be defined here

    

    Enlistment.belongsTo(models.SubjectOffering, {
      foreignKey: 'subjectOffering',
      as: 'subject-offering'
    }); 

    Enlistment.belongsTo(models.SchoolYear, {
      foreignKey: 'schoolYear',
      as: 'school-year'
    }); 

    Enlistment.hasOne(models.Student, {
      foreignKey: 'idNumber',
      sourceKey: 'studentId',
      as: 'student'
    })

    Enlistment.hasMany(models.EnlistmentSubjects, {
      foreignKey: 'enlistment',
      targetKey: 'id',
      as: 'enlistment-subjects'
    })

  };
  return Enlistment;
};