'use strict';
module.exports = (sequelize, DataTypes) => {
  const SchoolYear = sequelize.define('SchoolYear', {
    schoolYear: DataTypes.STRING,
    semester: DataTypes.STRING,
    current: DataTypes.BOOLEAN
  }, {});
  SchoolYear.associate = function(models) {
    // associations can be defined here
    SchoolYear.hasMany(models.SubjectOffering, {
      foreignKey: 'schoolYear',
      as: 'subject-offering'
    });

    SchoolYear.hasMany(models.Enlistment, {
      foreignKey: 'schoolYear',
      as: 'enlistments'
    });

  };
  return SchoolYear;
};