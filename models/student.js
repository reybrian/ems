'use strict';
module.exports = (sequelize, DataTypes) => {
  const Student = sequelize.define('Student', {
    //student
    idNumber: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    salutation: DataTypes.STRING,
    lastName: DataTypes.STRING,
    firstName: DataTypes.STRING,
    middleName: DataTypes.STRING,
    suffix: DataTypes.STRING,
    birthday: DataTypes.DATE,
    gender: DataTypes.STRING,
    civilStatus: DataTypes.STRING,
    religion: DataTypes.STRING,
    tribe: DataTypes.STRING,
    primary: DataTypes.STRING,
    primaryYearGraduated: DataTypes.STRING,
    juniorHighSchool: DataTypes.STRING,
    juniorHighSchoolYearGraduated: DataTypes.STRING,
    seniorHighSchool: DataTypes.STRING,
    seniorHighSchoolYearGraduated: DataTypes.STRING,
    underGraduate: DataTypes.STRING,
    underGraduateYearGraduated: DataTypes.STRING,
    mobileNumber: DataTypes.STRING,
    telephone: DataTypes.STRING,
    alternateEmail: DataTypes.STRING,
    region: DataTypes.STRING,
    city: DataTypes.STRING,
    barangay: DataTypes.STRING,
    streetAddress: DataTypes.STRING,
    postalCode: DataTypes.STRING,
    profileImagePath: DataTypes.STRING,
    //mother
    motherName: DataTypes.STRING,
    mregion: DataTypes.STRING,
    mcity: DataTypes.STRING,
    mbarangay: DataTypes.STRING,
    mstreetAddress: DataTypes.STRING,
    mpostalCode: DataTypes.STRING,
    memailAddress: DataTypes.STRING,
    mcontactNumber: DataTypes.STRING,
    //father
    fatherName: DataTypes.STRING,
    fregion: DataTypes.STRING,
    fcity: DataTypes.STRING,
    fbarangay: DataTypes.STRING,
    fstreetAddress: DataTypes.STRING,
    fpostalCode: DataTypes.STRING,
    femailAddress: DataTypes.STRING,
    fcontactNumber: DataTypes.STRING,
    //spouse
    ssalutation: DataTypes.STRING,
    slastName: DataTypes.STRING,
    sfirstName: DataTypes.STRING,
    smiddleName: DataTypes.STRING,
    ssuffix: DataTypes.STRING,
    sregion: DataTypes.STRING,
    scity: DataTypes.STRING,
    sbarangay: DataTypes.STRING,
    sstreetAddress: DataTypes.STRING,
    spostalCode: DataTypes.STRING,
    semailAddress: DataTypes.STRING,
    scontactNumber: DataTypes.STRING
   
  }, {});
  Student.associate = function(models) {
    // associations can be defined here
   
      Student.hasOne(models.Account, {
        foreignKey: 'idNumber',
        as: 'account',
      });

      Student.hasMany(models.Enlistment, {
        foreignKey: 'studentId',
        targetKey: 'idNumber',
        as: 'enlistments'
      });

  };
  return Student;
};