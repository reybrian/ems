'use strict';
module.exports = (sequelize, DataTypes) => {
  const Clerk = sequelize.define('Clerk', {
    idNumber: {
      type: DataTypes.STRING,
      primaryKey: true
    },
    salutation: DataTypes.STRING,
    lastName: DataTypes.STRING,
    firstName: DataTypes.STRING,
    middleName: DataTypes.STRING,
    suffix: DataTypes.STRING,
    birthday: DataTypes.DATE,
    gender: DataTypes.STRING,
    civilStatus: DataTypes.STRING,
    collegeCode: DataTypes.STRING,
    departmentCode: DataTypes.STRING,
    mobileNumber: DataTypes.STRING,
    telephone: DataTypes.STRING,
    alternateEmail: DataTypes.STRING,
    region: DataTypes.STRING,
    city: DataTypes.STRING,
    barangay: DataTypes.STRING,
    streetAddress: DataTypes.STRING,
    postalCode: DataTypes.STRING,
    clerkType: DataTypes.STRING,
    profileImagePath: DataTypes.STRING,
  }, {});
  Clerk.associate = function(models) {
    // associations can be defined here
   
      Clerk.hasOne(models.Account, {
        foreignKey: 'idNumber',
        as: 'account'
      });

  };
  return Clerk;
};