'use strict';
module.exports = (sequelize, DataTypes) => {
  const EnlistmentSubjects = sequelize.define('EnlistmentSubjects', {
    enlistment: DataTypes.INTEGER,
    schoolYear: DataTypes.INTEGER,
    subjectOffering: DataTypes.INTEGER,
    grade: DataTypes.STRING,
    completion: DataTypes.STRING,
    remark: DataTypes.STRING,
    locked: DataTypes.BOOLEAN
  }, {});
  EnlistmentSubjects.associate = function(models) {
    // associations can be defined here
    EnlistmentSubjects.hasOne(models.Enlistment, {
      foreignKey: 'id',
      sourceKey: 'enlistment',
      targetKey: 'enlistment',
      as: 'studentenlistment'
    })

    EnlistmentSubjects.hasOne(models.SubjectOffering, {
      foreignKey: 'id',
      sourceKey: 'subjectOffering',
      targetKey: 'subjectOffering',
      as: 'subjectoffering'
    })

  };
  return EnlistmentSubjects;
};