'use strict';
module.exports = (sequelize, DataTypes) => {
  const Classroom = sequelize.define('Classroom', {
    roomCode: DataTypes.STRING,
    building: DataTypes.STRING,
    collegeCode: DataTypes.STRING,
    active: DataTypes.BOOLEAN
  }, {
    defaultScope: {
      attributes: { exclude: ['classroom'] }
    }
  });
  Classroom.associate = function(models) {
    // associations can be defined here
    Classroom.belongsTo(models.College, {
      foreignKey: 'collegeCode',
      targetKey: 'collegeCode',
      as: 'college'
    })
  };
  return Classroom;
};