'use strict';
module.exports = (sequelize, DataTypes) => {
  const Form = sequelize.define('Form', {
    name: DataTypes.STRING,
    description: DataTypes.STRING,
    filePath: DataTypes.STRING
  }, {});
  Form.associate = function(models) {
    // associations can be defined here
  };
  return Form;
};