'use strict';
module.exports = (sequelize, DataTypes) => {
  const SubjectOffering = sequelize.define('SubjectOffering', {
    schoolYear: DataTypes.INTEGER,
    subjectCode: DataTypes.STRING,
    section: DataTypes.STRING,
    blockCode: DataTypes.STRING,
    deliveringDepartment: DataTypes.STRING,
    faculty: DataTypes.STRING,
    monday: DataTypes.BOOLEAN,
    tuesday: DataTypes.BOOLEAN,
    wednesday: DataTypes.BOOLEAN,
    thursday: DataTypes.BOOLEAN,
    friday: DataTypes.BOOLEAN,
    startTime: DataTypes.TIME,
    endTime: DataTypes.TIME,
    slots: DataTypes.INTEGER,
    slotsTaken: DataTypes.INTEGER,
    classroom: DataTypes.INTEGER,
    fee: DataTypes.INTEGER
  }, {});
  SubjectOffering.associate = function(models) {
    // associations can be defined here
    SubjectOffering.belongsTo(models.Subject,{
      foreignKey: 'subjectCode',
      targetKey: 'subjectCode',
      as: 'subject'
    });

    SubjectOffering.belongsTo(models.SchoolYear,{
      foreignKey: 'schoolYear',
      as: 'school-year'
    });

    SubjectOffering.hasOne(models.Faculty, {
      foreignKey: 'idNumber',
      sourceKey: 'faculty',
      as: 'subject-faculty'
    });

    // SubjectOffering.belongsTo(models.Faculty,{
    //   foreignKey: 'faculty',
    //   targetKey: 'idNumber',
    //   as: 'subject-faculty'
    // });

    SubjectOffering.hasOne(models.Classroom,{
      foreignKey: 'id',
      sourceKey: 'classroom',
      as: 'subjectclassroom'
    });

    SubjectOffering.belongsTo(models.Department, {
      foreignKey: 'deliveringDepartment',
      targetKey: 'departmentCode',
      as: 'delivering-department'
    });

    SubjectOffering.hasMany(models.Enlistment, {
      foreignKey: 'subjectOffering',
      as: 'enlistments'
    });

  };
  return SubjectOffering;
};