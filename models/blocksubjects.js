'use strict';
module.exports = (sequelize, DataTypes) => {
  const BlockSubjects = sequelize.define('BlockSubjects', {
    blockCode: DataTypes.STRING,
    schoolYear: DataTypes.INTEGER,
    subjectOffering: DataTypes.INTEGER,
    active: DataTypes.BOOLEAN
  }, {});
  BlockSubjects.associate = function(models) {
    // associations can be defined here
    BlockSubjects.hasOne(models.SchoolYear, {
      foreignKey: 'schoolYear',
      as: 'sy'
    });

    BlockSubjects.hasOne(models.SubjectOffering, {
      foreignKey:'id',
      as: 'subject-offering'
    });

  };
  return BlockSubjects;
};