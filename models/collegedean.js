'use strict';
module.exports = (sequelize, DataTypes) => {
  const CollegeDean = sequelize.define('CollegeDean', {
    collegeCode: DataTypes.STRING,
    dean: DataTypes.STRING,
    assistantDean: DataTypes.STRING
  }, {});
  CollegeDean.associate = function(models) {
    // associations can be defined here
    CollegeDean.hasOne(models.Faculty,{
      foreignKey: 'idNumber',
      sourceKey: 'dean',
      targetKey: 'dean',
      as: 'collegedean'
    });

    CollegeDean.hasOne(models.Faculty,{
      foreignKey: 'idNumber',
      sourceKey: 'assistantDean',
      targetKey: 'assistantDean',
      as: 'collegeassistantdean'
    });
  };
  return CollegeDean;
};