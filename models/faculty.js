'use strict';
module.exports = (sequelize, DataTypes) => {
  const Faculty = sequelize.define('Faculty', {
    idNumber: {
      type: DataTypes.STRING,
      primaryKey: true,
    },
    salutation: DataTypes.STRING,
    firstName: DataTypes.STRING,
    middleName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    suffix: DataTypes.STRING,
    birthday: DataTypes.DATE,
    gender: DataTypes.STRING,
    civilStatus: DataTypes.STRING,
    primary: DataTypes.STRING,
    primaryYearGraduated: DataTypes.STRING,
    juniorHighSchool: DataTypes.STRING,
    juniorHighSchoolYearGraduated: DataTypes.STRING,
    seniorHighSchool: DataTypes.STRING,
    seniorHighSchoolYearGraduated: DataTypes.STRING,
    undergraduate: DataTypes.STRING,
    undergraduateYearGraduated: DataTypes.STRING,
    undergraduateDegree: DataTypes.STRING,
    masteral: DataTypes.STRING,
    masteralYearGraduated: DataTypes.STRING,
    masteralDegree: DataTypes.STRING,
    doctorate: DataTypes.STRING,
    doctorateYearGraduated: DataTypes.STRING,
    doctorateDegree: DataTypes.STRING,
    postGraduate: DataTypes.STRING,
    postGraduateYearGraduated: DataTypes.STRING,
    postGraduateDegree: DataTypes.STRING,
    collegeCode: DataTypes.STRING,
    departmentCode: DataTypes.STRING,
    rank: DataTypes.STRING,
    step: DataTypes.STRING,
    mobileNumber: DataTypes.STRING,
    telephoneNumber: DataTypes.STRING,
    emailAddress: DataTypes.STRING,
    streetAddress: DataTypes.STRING,
    baranggay: DataTypes.STRING,
    city: DataTypes.STRING,
    region: DataTypes.STRING,
    postalCode: DataTypes.STRING,
    profileImagePath: DataTypes.STRING

  }, {});
  Faculty.associate = function(models) {
    // associations can be defined here

    Faculty.hasMany(models.SubjectOffering, {
      foreignKey: 'faculty',
      targetKey: 'idNumber',
      sourceKey: 'idNumber'
    });

   
    Faculty.hasOne(models.Account, {
      foreignKey: 'idNumber',
      as: 'account',
    });
    
  };

  return Faculty;
};