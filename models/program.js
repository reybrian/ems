'use strict';
module.exports = (sequelize, DataTypes) => {
  const Program = sequelize.define('Program', {
    programCode: DataTypes.STRING,
    programDesc: DataTypes.STRING,
    programType: DataTypes.STRING,
    deliveringDepartment: DataTypes.STRING,
    totalSemester: DataTypes.INTEGER,
    totalUnits: DataTypes.INTEGER,
    active: DataTypes.BOOLEAN
  }, {
    defaultScope: {
      attributes: { exclude: ['forDepartment'] }
    }
  });
  Program.associate = function(models) {
    // associations can be defined here
    Program.hasMany(models.ProgramMajor, {
      foreignKey: 'forProgram',
      sourceKey: 'programCode',
      as: 'majors'
    });

    Program.belongsTo(models.Department, {
      foreignKey: 'deliveringDepartment',
      targetKey: 'departmentCode',
      as: 'department'
    })

  };
  return Program;
};