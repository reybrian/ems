'use strict';
module.exports = (sequelize, DataTypes) => {
  const College = sequelize.define('College', {
    collegeCode: DataTypes.STRING,
    collegeDescription: DataTypes.STRING,
    active: DataTypes.BOOLEAN
  }, {});
  College.associate = function(models) {
    // associations can be defined here

    College.belongsTo(models.CollegeDean, {
      foreignKey: 'collegeCode',
      targetKey: 'collegeCode',
      as: 'colldean'
    });

    College.hasMany(models.Block, {
      foreignKey: 'collegeCode',
      targetKey: 'collegeCode',
      as: 'blocks'
    });

    College.hasMany(models.Classroom, {
      foreignKey: 'collegeCode',
      targetKey: 'collegeCode',
      as: 'classrooms'
    });

    College.hasMany(models.Department, {
      foreignKey: 'collegeCode',
      sourceKey: 'collegeCode',
      as: 'departments'
    });

  };
  return College;
};