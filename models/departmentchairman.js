'use strict';
module.exports = (sequelize, DataTypes) => {
  const DepartmentChairman = sequelize.define('DepartmentChairman', {
    departmentCode: DataTypes.STRING,
    chairman: DataTypes.STRING
  }, {});
  DepartmentChairman.associate = function(models) {
    // associations can be defined here
    DepartmentChairman.hasOne(models.Faculty, {
      foreignKey: 'idNumber',
      targetKey: 'chairman',
      sourceKey: 'chairman',
      as: 'department'
    });
  };
  return DepartmentChairman;
};