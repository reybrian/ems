'use strict';
module.exports = (sequelize, DataTypes) => {
  const ProgramMajor = sequelize.define('ProgramMajor', {
    majorDescription: DataTypes.STRING,
    forProgram: DataTypes.STRING,
    active: DataTypes.BOOLEAN
  }, {});
  ProgramMajor.associate = function(models) {
    // associations can be defined here
    ProgramMajor.belongsTo(models.Program,{
      foreignKey: 'forProgram',
      targetKey: 'programCode',
      as: 'program'
    });
  };
  return ProgramMajor;
};