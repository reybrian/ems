'use strict';
module.exports = (sequelize, DataTypes) => {
  const Block = sequelize.define('Block', {
    blockCode: DataTypes.STRING,
    forCollege: DataTypes.STRING,
    forDepartment: DataTypes.STRING
  }, {
    defaultScope: {
      attributes: { exclude: ['collegeCode'] }
    }
  });
  Block.associate = function(models) {
    // associations can be defined here
    Block.belongsTo(models.College, {
      foreignKey: 'forCollege',
      targetKey: 'collegeCode',
      as: 'college'
    });

    Block.belongsTo(models.Department, {
      foreignKey: 'forDepartment',
      targetKey: 'departmentCode',
      as: 'department'
    })
  };
  return Block;
};