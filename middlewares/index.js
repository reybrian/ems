
const multer = require('multer');
const staffInformationController = require('../controllers').staffInformation;
const studentInformationController = require('../controllers').studentInformation

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
      cb(null, './uploads/');
    },
    filename: function(req, file, cb) {
      cb(null, new Date().toISOString().replace(/:/g, '-') + file.originalname);
    }
  })

const upload = multer({
    storage: storage,
    
    limits: {
      fileSize: 1024 * 1024 * 5
    }
  })
  
module.exports = {
    isAdmin(req, res, next) {

        if(!req.session.user){
            res.redirect('/')
            if(req.session.user.roleID !== 1){
                res.redirect('/')
            }
        }
        next();
      },
      isStudent(req, res, next) {

        if(!req.session.user){
            res.redirect('/')
            if(req.session.user.roleID !== 2){
                res.redirect('/')
            }
        }
        next();
      },
      upload,
      async loadSession(req,res,next){
        if (req.session.user) {
          let data
          if(req.session.user.roleID === 1){
            
            res.locals.account = req.session.user
            data = await staffInformationController.getClerkByIdNumberReturn(req.session.user.idNumber)
            
            res.locals.userInfo = data
            
          } else {
            res.locals.account = req.session.user
            data = await studentInformationController.getStudentByIdNumberReturn(req.session.user.idNumber)
            
            res.locals.userInfo = data
            console.log(res.locals)
          }
          
        }
        next();
      }
}