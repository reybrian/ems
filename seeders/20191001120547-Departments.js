'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */

   return queryInterface.bulkInsert('Departments', [
    {
     
      departmentCode: 'BSIT',
      departmentDescription: 'Bachelor of Science in Information Technology',
      collegeCode: 'CNSM',
      active: true,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      departmentCode: 'BSMATH',
      departmentDescription: 'Bachelor of Science in Mathematics',
      collegeCode: 'CNSM',
      active: true,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      departmentCode: 'ABE',
      departmentDescription: 'Agricultural and Biosystems Engineering',
      collegeCode: 'COA',
      active: true,
      createdAt: new Date(),
      updatedAt: new Date()
    }
  
  ], {});
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
