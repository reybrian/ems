'use strict';
const bcrypt = require('bcrypt');

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */


   return queryInterface.bulkInsert('Accounts', [
    {
      schoolEmail: 'adminDemo@msugensan.edu.ph',
      password: bcrypt.hashSync('secret123', 5),
      idNumber: null,
      roleID: 1,
      isActive: true,
      createdAt: new Date(),
      updatedAt: new Date()
    }
   
  ], {});

  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
