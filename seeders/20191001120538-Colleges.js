'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('People', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
      

   return queryInterface.bulkInsert('Colleges', [
    {
      collegeCode: 'CNSM',
      collegeDesc: 'College of Natural Sciences and Mathematics',
      active: true,
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      collegeCode: 'COA',
      collegeDesc: 'College of Agriculture',
      active: true,
      createdAt: new Date(),
      updatedAt: new Date()
    },
  
  ], {});

  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('People', null, {});
    */
  }
};
