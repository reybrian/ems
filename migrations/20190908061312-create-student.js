'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Students', {
      //student
      idNumber: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING
      },
      salutation: {
        type: Sequelize.STRING
      },
      lastName: {
        type: Sequelize.STRING
      },
      firstName: {
        type: Sequelize.STRING
      },
      middleName: {
        type: Sequelize.STRING
      },
      suffix: {
        type: Sequelize.STRING 
      },
      birthday: {
        type: Sequelize.DATE
      },
      gender: {
        type: Sequelize.STRING
      },
      civilStatus: {
        type: Sequelize.STRING
      },
      religion: {
        type: Sequelize.STRING,      
      },
      tribe: {
        type: Sequelize.STRING,      
      },
      primary: {
        type: Sequelize.STRING
      },
      primaryYearGraduated: {
        type: Sequelize.STRING,
      },
      juniorHighSchool: {
        type: Sequelize.STRING
      },
      juniorHighSchoolYearGraduated: {
        type: Sequelize.STRING,
      },
      seniorHighSchool: {
        type: Sequelize.STRING
      },
      seniorHighSchoolYearGraduated: {
        type: Sequelize.STRING,
      },
      underGraduate: {
        type: Sequelize.STRING,
      },
      underGraduateYearGraduated: {
        type: Sequelize.STRING,
      },
      mobileNumber: {
        type: Sequelize.STRING,
      },
      telephone: {
        type: Sequelize.STRING,
      },
      alternateEmail: {
        type: Sequelize.STRING,
      },
      region: {
        type: Sequelize.STRING,        
      },
      city: {
        type: Sequelize.STRING,      
      },
      barangay: {
        type: Sequelize.STRING,      
      },
      streetAddress: {
        type: Sequelize.STRING,
      },
      postalCode: {
        type: Sequelize.STRING,
      },
      profileImagePath: {
        type: Sequelize.STRING,
      },
      //mother
      motherName: {
        type: Sequelize.STRING,
      },
      mregion: {
        type: Sequelize.STRING,  
      },
      mcity: {
        type: Sequelize.STRING,   
      },
      mbarangay: {
        type: Sequelize.STRING, 
      },
      mstreetAddress: {
        type: Sequelize.STRING,
      },
      mpostalCode: {
        type: Sequelize.STRING,
      },
      memailAddress: {
        type: Sequelize.STRING,
      },
      mcontactNumber: {
        type: Sequelize.STRING,
      },
      //father
      fatherName: {
        type: Sequelize.STRING,
      },
      fregion: {
        type: Sequelize.STRING,  
      },
      fcity: {
        type: Sequelize.STRING,   
      },
      fbarangay: {
        type: Sequelize.STRING, 
      },
      fstreetAddress: {
        type: Sequelize.STRING,
      },
      fpostalCode: {
        type: Sequelize.STRING,
      },
      femailAddress: {
        type: Sequelize.STRING,
      },
      fcontactNumber: {
        type: Sequelize.STRING,
      },
      //spouse
      ssalutation: {
        type: Sequelize.STRING,
      },
      slastName: {
        type: Sequelize.STRING,
      },
      sfirstName: {
        type: Sequelize.STRING,
      },
      smiddleName: {
        type: Sequelize.STRING,
      },
      ssuffix: {
        type: Sequelize.STRING,
      },
      sregion: {
        type: Sequelize.STRING,  
      },
      scity: {
        type: Sequelize.STRING,   
      },
      sbarangay: {
        type: Sequelize.STRING,   
      },
      sstreetAddress: {
        type: Sequelize.STRING,
      },
      spostalCode: {
        type: Sequelize.STRING,
      },
      semailAddress: {
        type: Sequelize.STRING,
      },
      scontactNumber: {
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Students');
  }
};