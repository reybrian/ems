'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Faculties', {
      idNumber: {
        type: Sequelize.STRING,
        allowNull: false,
        primaryKey: true,
      },
      salutation: {
        type: Sequelize.STRING
      },
      firstName: {
        type: Sequelize.STRING
      },
      middleName: {
        type: Sequelize.STRING
      },
      lastName: {
        type: Sequelize.STRING
      },
      suffix: {
        type: Sequelize.STRING 
      },
      birthday: {
        type: Sequelize.DATE
      },
      gender: {
        type: Sequelize.STRING
      },
      civilStatus: {
        type: Sequelize.STRING
      },
      primary: {
        type: Sequelize.STRING
      },
      primaryYearGraduated: {
        type: Sequelize.STRING,
      },
      juniorHighSchool: {
        type: Sequelize.STRING
      },
      juniorHighSchoolYearGraduated: {
        type: Sequelize.STRING,
      },
      seniorHighSchool: {
        type: Sequelize.STRING
      },
      seniorHighSchoolYearGraduated: {
        type: Sequelize.STRING,
      },
      undergraduate: {
        type: Sequelize.STRING
      },
      undergraduateYearGraduated: {
        type: Sequelize.STRING,
      },
      undergraduateDegree: {
        type: Sequelize.STRING
      },
      masteral: {
        type: Sequelize.STRING
      },
      masteralYearGraduated: {
        type: Sequelize.STRING,
      },
      masteralDegree: {
        type: Sequelize.STRING
      },
      doctorate: {
        type: Sequelize.STRING
      },
      doctorateYearGraduated: {
        type: Sequelize.STRING,
      },
      doctorateDegree: {
        type: Sequelize.STRING
      },
      postGraduate: {
        type: Sequelize.STRING
      },
      postGraduateYearGraduated: {
        type: Sequelize.STRING,
      },
      postGraduateDegree: {
        type: Sequelize.STRING
      },
      collegeCode: {
        type: Sequelize.STRING,
        references: {
          model: 'Colleges', 
          key: 'collegeCode', 
        }  
      },
      departmentCode: {
        type: Sequelize.STRING,
        references: {
          model: 'Departments', 
          key: 'departmentCode', 
        }  
      },
      rank: {
        type: Sequelize.STRING
      },
      step: {
        type: Sequelize.STRING
      },
      mobileNumber: {
        type: Sequelize.STRING
      },
      telephoneNumber: {
        type: Sequelize.STRING
      },
      emailAddress: {
        type: Sequelize.STRING
      },
      streetAddress: {
        type: Sequelize.STRING
      },
      baranggay: {
        type: Sequelize.STRING
      },
      city: {
        type: Sequelize.STRING
      },
      region: {
        type: Sequelize.STRING
      },
      postalCode: {
        type: Sequelize.STRING,
      },
      profileImagePath: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }



    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Faculties');
  }
};