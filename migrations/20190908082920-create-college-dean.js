'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('CollegeDeans', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      collegeCode: {
        type: Sequelize.STRING,
        references: {
          model: 'Colleges',
          key: 'collegeCode'
        }
      },
      dean: {
        type: Sequelize.STRING,
        references: {
          model: 'Faculties',
          key: 'idNumber'
        }
      },
      assistantDean: {
        type: Sequelize.STRING,
        references: {
          model: 'Faculties',
          key: 'idNumber'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('CollegeDeans');
  }
};