'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('SubjectOfferings', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      schoolYear: {
        type: Sequelize.INTEGER,
        references: {
          model: 'SchoolYears',
          key: 'id'
        }
      },
      subjectCode: {
        type: Sequelize.STRING,
        references: {
          model: 'Subjects',
          key: 'subjectCode'
        }
      },
      section: {
        type: Sequelize.STRING
      },
      blockCode: {
        type: Sequelize.STRING,
        references: {
          model: 'Blocks',
          key: 'blockCode'
        }
      },
      deliveringDepartment: {
        type: Sequelize.STRING,
        references: {
          model: 'Departments',
          key: 'departmentCode'
        }
      },
      faculty: {
        type: Sequelize.STRING,
        references: {
          model: 'Faculties',
          key: 'idNumber'
        }
      },
      monday: {
        type: Sequelize.BOOLEAN
      },
      tuesday: {
        type: Sequelize.BOOLEAN
      },
      wednesday: {
        type: Sequelize.BOOLEAN
      },
      thursday: {
        type: Sequelize.BOOLEAN
      },
      friday: {
        type: Sequelize.BOOLEAN
      },
      startTime: {
        type: Sequelize.TIME
      },
      endTime: {
        type: Sequelize.TIME
      },
      slots: {
        type: Sequelize.INTEGER
      },
      slotsTaken: {
        type: Sequelize.INTEGER
      },
      classroom: {
        type: Sequelize.INTEGER
      },
      fee: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('SubjectOfferings');
  }
};