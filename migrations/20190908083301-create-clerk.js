'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Clerks', {
      idNumber: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING
      },
      salutation: {
        type: Sequelize.STRING
      },
      lastName: {
        type: Sequelize.STRING
      },
      firstName: {
        type: Sequelize.STRING
      },
      middleName: {
        type: Sequelize.STRING
      },
      suffix: {
        type: Sequelize.STRING 
      },
      birthday: {
        type: Sequelize.DATE
      },
      gender: {
        type: Sequelize.STRING
      },
      civilStatus: {
        type: Sequelize.STRING
      },
      collegeCode: {
        type: Sequelize.STRING,
        references: {
          model: 'Colleges', 
          key: 'collegeCode', 
        }  
      },
      departmentCode: {
        type: Sequelize.STRING,
        references: {
          model: 'Departments', 
          key: 'departmentCode', 
        }  
      },
      mobileNumber: {
        type: Sequelize.STRING,
      },
      telephone: {
        type: Sequelize.STRING,
      },
      alternateEmail: {
        type: Sequelize.STRING,
      },
      region: {
        type: Sequelize.STRING,
        
      },
      city: {
        type: Sequelize.STRING,
        
      },
      barangay: {
        type: Sequelize.STRING,
        
      },
      streetAddress: {
        type: Sequelize.STRING,
      },
      postalCode: {
        type: Sequelize.STRING,
      },
      clerkType: {
        type: Sequelize.STRING,
      },
      profileImagePath: {
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Clerks');
  }
};