'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Enlistments', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      schoolYear: {
        type: Sequelize.INTEGER,
        references: {
          model: 'SchoolYears',
          key: 'id'
        }
      },
      studentId: {
        type: Sequelize.STRING,
        references: {
          model: 'Students',
          key: 'idNumber'
        }
      },
      yearLevel: {
        type: Sequelize.INTEGER
      },
      programCode: {
        type: Sequelize.STRING,
        references: {
          model: 'Programs',
          key: 'programCode'
        }
      },
      majorCode: {
        type: Sequelize.STRING
      },
      graduating: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Enlistments');
  }
};