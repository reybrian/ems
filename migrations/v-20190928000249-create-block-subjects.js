'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('BlockSubjects', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      blockCode: {
        type: Sequelize.STRING,
        references: {
          model: 'Blocks',
          key: 'blockCode'
        }
      },
      schoolYear: {
        type: Sequelize.INTEGER,
        references: {
          model: 'SchoolYears',
          key: 'id'
        }
      },
      subjectOffering: {
        type: Sequelize.INTEGER,
        references: {
          model: 'SubjectOfferings',
          key: 'id'
        }
      },
      active: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('BlockSubjects');
  }
};