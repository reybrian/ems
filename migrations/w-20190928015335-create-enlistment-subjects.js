'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('EnlistmentSubjects', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      enlistment: {
        type: Sequelize.INTEGER,
        references: {
          model: 'Enlistments',
          key: 'id'
        }
      },
      schoolYear: {
        type: Sequelize.INTEGER,
        references: {
          model: 'SchoolYears',
          key: 'id'
        }
      },
      subjectOffering: {
        type: Sequelize.INTEGER,
        references: {
          model: 'SubjectOfferings',
          key: 'id'
        }
      },
      
      grade: {
        type: Sequelize.STRING
      },
      completion: {
        type: Sequelize.STRING
      },
      remark: {
        type: Sequelize.STRING
      },
      locked: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('EnlistmentSubjects');
  }
};