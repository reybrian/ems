'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Programs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      programCode: {
        type: Sequelize.STRING,
        unique: true
      },
      programDesc: {
        type: Sequelize.STRING
      },
      programType: {
        type: Sequelize.STRING
      },
      deliveringDepartment: {
        type: Sequelize.STRING,
        references: {
          model: 'Departments', 
          key: 'departmentCode', 
        }  
      },
      totalSemester: {
        type: Sequelize.INTEGER
      },
      totalUnits: {
        type: Sequelize.INTEGER
      },
      active: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Programs');
  }
};