'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Departments', {
      departmentCode: {
        type: Sequelize.STRING,
        allowNull: false,
        primaryKey: true,
      },
      departmentDescription: {
        type: Sequelize.STRING
      },
      collegeCode: {
        type: Sequelize.STRING,
        references: {
          model: 'Colleges', 
          key: 'collegeCode', 
        }  
      },
      active: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Departments');
  }
};