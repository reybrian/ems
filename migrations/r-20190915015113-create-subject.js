'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Subjects', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      subjectCode: {
        type: Sequelize.STRING,
        unique: true
      },
      isGPA: {
        type: Sequelize.BOOLEAN
      },
      subjectDescription: {
        type: Sequelize.STRING
      },
      totalWeeks: {
        type: Sequelize.INTEGER
      },
      totalHours: {
        type: Sequelize.INTEGER
      },
      lectureUnits: {
        type: Sequelize.INTEGER
      },
      labUnits: {
        type: Sequelize.INTEGER
      },
      totalUnits: {
        type: Sequelize.INTEGER
      },
      labFee: {
        type: Sequelize.INTEGER
      },
      facultyCredit: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Subjects');
  }
};