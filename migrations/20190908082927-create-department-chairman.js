'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('DepartmentChairmans', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      departmentCode: {
        type: Sequelize.STRING,
        references:{ 
          model: 'Departments',
          key: 'departmentCode'
        }
      },
      departmentChairman: {
        type: Sequelize.STRING,
        references:{ 
          model: 'Faculties',
          key: 'idNumber'
        }
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('DepartmentChairmans');
  }
};