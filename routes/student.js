var express = require('express');
var router = express.Router();
var createError = require('http-errors');

router.get('/dashboard', (req, res) => {
    res.render('student/pages/dashboard', {title: 'Dashboard'});
})

router.get('/profile', (req, res) => {
    res.render('student/pages/profile', {title: 'Profile', idNumber: req.session.user.idNumber});
})

router.get('/account-settings', async (req, res) => {
   
    res.render('student/pages/account-settings', {title: 'Settings', idNumber: req.session.user.idNumber});
});
module.exports = router