var express = require('express');
var router = express.Router();
var createError = require('http-errors');

const middlewares = require('../middlewares')

const studentInformationController = require('../controllers').studentInformation;
const facultyInformationController = require('../controllers').facultyInformation;
const staffInformationController = require('../controllers').staffInformation;
const collegeController = require('../controllers/college');
const departmentController = require('../controllers/department');
const classroomController = require('../controllers/classroom');
const subjectController = require('../controllers/subject');
const programController = require('../controllers/program');
const blockController = require('../controllers/block');
const schoolYearController = require('../controllers/school-year');
const subjectOfferingController = require('../controllers/subject-offering');
const blockSubjectsController = require('../controllers/block-subjects');
const enlistmentController = require('../controllers/enlistment');
const enlistmentSubjectController = require('../controllers/enlistment-subjects');
const programMajorController = require('../controllers/program-major');
const accountsManagementController = require('../controllers').accountsManagement
const formController = require('../controllers').form
const calendarController = require('../controllers').calendar

//APIs
router.post('/studentInformation', studentInformationController.postStudent);
router.get('/studentInformation', studentInformationController.getAllStudent);
router.get('/studentInformation/:idNumber', studentInformationController.getStudentByIdNumber);
router.put('/studentInformation/:idNumber', studentInformationController.putStudent);

router.post('/facultyInformation', facultyInformationController.postFaculty);
router.get('/facultyInformation', facultyInformationController.getAllFaculty);
router.get('/facultyInformation/:idNumber', facultyInformationController.getFacultyByIdNumber);
router.put('/facultyInformation/:idNumber', facultyInformationController.putFaculty);

router.post('/staffInformation', staffInformationController.postClerk);
router.get('/staffInformation', staffInformationController.getAllClerk);
router.get('/staffInformation/:idNumber', staffInformationController.getClerkByIdNumber);
router.put('/staffInformation/:idNumber', staffInformationController.putClerk);

router.post('/accountsManagement', accountsManagementController.postStudentAccount);
router.get('/accountsManagement/:idNumber', accountsManagementController.getAccountbyIdNumber);
router.put('/accountsManagement/:idNumber', accountsManagementController.putStatus);
router.put('/accountsManagement/resetPassword/:idNumber', accountsManagementController.resetPass);

router.post('/form', middlewares.upload.single('file') ,formController.postForm);
router.get('/form/:id', formController.getFormById);
router.delete('/form/:id', formController.deleteForm);
router.put('/form/:id', middlewares.upload.single('file') ,formController.putForm);

router.post('/calendar', calendarController.postEvent)

//college
router.post('/api/college/add', collegeController.createCollege);
router.get('/colleges', collegeController.listAll);
router.get('/api/college/:id', collegeController.fetchCollege);
router.put('/api/college/:id', collegeController.updateCollege);
router.put('/api/college/activate/:collegeCode', collegeController.activateCollege);
router.put('/api/college/deactivate/:collegeCode', collegeController.deactivateCollege);

//department
router.get('/departments', departmentController.listAll);
router.post('/api/department/add', departmentController.createDepartment);
router.put('/api/department/activate/:departmentCode', departmentController.activateDepartment);
router.put('/api/department/deactivate/:departmentCode', departmentController.deactivateDepartment);
router.get('/api/department/:id', departmentController.fetchDepartment);
router.put('/api/department/:id', departmentController.updateDepartment);

//classroom
router.get('/api/classroom/', classroomController.listAll);
router.post('/api/classroom/add', classroomController.createRoom);
router.get('/api/classroom/:roomCode', classroomController.fetchRoom);
router.put('/api/classroom/activate/:roomCode', classroomController.activateRoom);
router.put('/api/classroom/deactivate/:roomCode', classroomController.deactivateRoom);
router.put('/api/classroom/:roomCode', classroomController.updateRoom);

//subject
router.get('/api/subject/', subjectController.listAll);
router.post('/api/subject/add', subjectController.createSubject);
router.get('/api/subject/:subjectCode', subjectController.fetchSubject);
router.put('/api/subject/:subjectCode', subjectController.updateSubject);

//program
router.get('/api/program/', programController.listAll);
router.post('/api/program/add', programController.createProgram);
router.get('/api/program/major/:programCode', programMajorController.getProgramMajors );
router.get('/api/program/:programCode', programController.fetchProgram);
router.put('/api/program/activate/:programCode', programController.activateProgram);
router.put('/api/program/deactivate/:programCode', programController.deactivateProgram);
router.put('/api/program/:programCode', programController.updateProgram);

//blocks
router.post('/api/block/add', blockController.createBlock);
router.get('/api/block/', blockController.listAll);
router.post('/api/block/subject/add', blockSubjectsController.addBlockSubject);
router.delete('/api/block/subject/remove', blockSubjectsController.removeBlockSubject);
router.get('/api/block/subject/:blockCode', blockSubjectsController.getBlockSubjects);
router.get('/api/block/:blockCode', blockController.fetchBlock);

//schoolyear
router.get('/api/sy/', schoolYearController.listAll);
router.get('/api/sy/current', schoolYearController.getCurrent);
router.post('/api/sy/add', schoolYearController.createSchoolyear);
router.put('/api/sy/close', schoolYearController.closeSemester);

//subject offering
router.post('/api/subject-offering/add', subjectOfferingController.createSubjectOffering);
router.get('/api/subject-offering', subjectOfferingController.listAll);
router.get('/api/subject-offering/current', subjectOfferingController.getCurrentSubjectOfferings);
router.put('/api/subject-offering/:id', subjectOfferingController.updateOffering);
router.get('/api/subject-offering/:id', subjectOfferingController.fetchOffering);

//enlistment
router.post('/api/enlistment/add', enlistmentController.addEnlistment);
router.put('/api/enlistment/:id', enlistmentController.updateEnlistment);
router.get('/api/enlistment/', enlistmentController.listAll);
router.get('/api/enlistment/current', enlistmentController.listAllCurrent);
router.delete('/api/enlistment/subject/remove', enlistmentSubjectController.removeSubjectEnlistment);
router.post('/api/enlistment/subject/add', enlistmentSubjectController.addEnlistmentSubject);
router.get('/api/enlistment/subject/:schoolYear/:enlistment', enlistmentSubjectController.getStudentSubjectsApi);
router.get('/api/enlistment/:id', enlistmentController.fetchEnlistment);



//Page loaders
router.get('/dashboard', async (req, res) => {
    res.render('admin/pages/dashboard', {title: 'Dashboard'});
});

router.get('/profile', async (req, res) => {
   
    res.render('admin/pages/profile', {title: 'Profile', idNumber: req.session.user.idNumber});
});

router.get('/account-settings', async (req, res) => {
   
    res.render('admin/pages/account-settings', {title: 'Settings', idNumber: req.session.user.idNumber});
});

router.get('/accounts-management', async (req, res) => {
    const accounts = await accountsManagementController.getAllAccounts()
    res.render('admin/pages/accounts', {title: 'Accounts', accounts: accounts});
});
router.get('/accounts-management/add/:type', async (req, res) => {
    if(req.params.type < 1 || req.params.type > 6 ||  isNaN(req.params.type)){
        throw new Error(createError(404))
    }

    let idNumbers;

    
    if(req.params.type === '2'){
         idNumbers = await studentInformationController.getAllStudent()
    }
    else if(req.params.type === '3'){
        idNumbers = await facultyInformationController.getAllFaculty()
    }
    else if(req.params.type === '5'){
        idNumbers = await staffInformationController.getAllClerkDepartmentClerk()
    }
    else{
        idNumbers = await staffInformationController.getAllClerkStaff()
    }


    res.render('admin/pages/account-new' ,{title: 'Add Account',type: req.params.type,idNumbers: idNumbers});
});

router.get('/calendar', (req, res) => {
    res.render('admin/pages/calendar',{title:'Calendar'});
});
router.get('/student-information', async (req, res) => {
    const students = await studentInformationController.getAllStudent()
    res.render('admin/pages/sis',{title:'Student Information', students:students});
});
router.get('/student-information/add', (req, res) => {
    res.render('admin/pages/student-new',{title:'Add Student'});
});
router.get('/student-information/edit/:idNumber', (req, res) => {
    res.render('admin/pages/student-edit',{title:'Edit Student', idNumber: req.params.idNumber});
});
router.get('/faculty-information', async (req, res) => {
    const faculties = await facultyInformationController.getAllFaculty()
    res.render('admin/pages/fis',{title:'Faculty Information', faculties: faculties});
});
router.get('/faculty-information/add', (req, res) => {
    res.render('admin/pages/faculty-new',{title:'Add Faculty'});
});

router.get('/faculty-information/edit/:idNumber', (req, res) => {
    res.render('admin/pages/faculty-edit',{title:'Edit Faculty', idNumber: req.params.idNumber});
});

router.get('/staff-information', async (req, res) => {
    const staffs = await staffInformationController.getAllClerk()
    res.render('admin/pages/cis',{title:'Staff Information', staffs: staffs});
});

router.get('/staff-information/add/:type', (req, res) => {
    if(req.params.type < 1 || req.params.type > 2 ||  isNaN(req.params.type)){
        throw new Error(createError(404))
    }
    console.log(req.params.type)
    res.render('admin/pages/staff-new',{title: req.params.type === '1' ? 'Add Staff' : 'Add Department Clerk', type: req.params.type});
});

router.get('/staff-information/edit/:idNumber/:type', (req, res) => {
    res.render('admin/pages/staff-edit',{
        
    title: req.params.type === '1' ? 'Edit Staff' : 'Edit Department Clerk'
    
    , idNumber: req.params.idNumber, type: req.params.type});
});

// router.get('/colleges', (req, res) => {
//     res.render('admin/pages/colleges',{title:'Colleges'});
// });

// router.get('/departments', (req, res) => {
//     res.render('admin/pages/departments',{title:'Departments'});
// });

router.get('/classrooms', (req, res) => {
    res.render('admin/pages/classrooms',{title:'Classrooms'});
});

router.get('/subjects', (req, res) => {
    res.render('admin/pages/subjects',{title:'Subjects'});
});


router.get('/programs', (req, res) => {
    res.render('admin/pages/programs',{title:'Programs'});
});

router.get('/offering', async (req, res) => {
    const currentSy = await schoolYearController.getCurrent();
    const subjectOffering = await subjectOfferingController.getCurrentSubjectOfferings();
    
    res.render('admin/pages/offering',{title:'Offering', subjectOffering, currentSy});
});

router.get('/offering-new',async (req, res) => {
    const classrooms = await classroomController.listAll();
    const departments = await departmentController.getAll();
    const currentSy = await schoolYearController.getCurrent();
    const subjects = await subjectController.listAll();
    const blocks = await blockController.listAll();
    const faculty = await facultyInformationController.getAll()
    res.render('admin/pages/offering-new',{
        title:'New Offering', 
        subjects: subjects, 
        blocks:blocks, 
        currentSy: currentSy, 
        departments: departments, 
        faculty:faculty,
        classrooms: classrooms
    });
});

router.get('/blocks', (req, res) => {
    res.render('admin/pages/blocks',{title:'Blocks'});
});

router.get('/advising', async (req, res) => {
    const program = await programController.listAll();
    const student = await studentInformationController.listAll();
    res.render('admin/pages/advising',{title:'Enlistment', student, program});
});

router.get('/enlistment', async (req, res) => {
    const program = await programController.listAll();
    //const student = await studentInformationController.listAll();
    const currentSy = await schoolYearController.getCurrent();
    const enlistment = await enlistmentController.listAllCurrent();
    res.render('admin/pages/enlistment',{title:'Enlistment', enlistment, currentSy, program});
});

router.get('/enrolledsubj-edit/:sy/:id/:sid', async (req, res) => {
    const enlistmentId = req.params.id;
    const studentInfo = await studentInformationController.fetchStudentByIdNumber(req.params.sid);
    const subjectOff = await subjectOfferingController.getCurrentSubjectOfferings();
    const enlistmentSubj = await enlistmentSubjectController.getStudentSubjects(req.params.sy, req.params.id);
    res.render('admin/pages/enrolledsubj-edit',{title:'Enlistment', enlistmentSubj, subjectOff, studentInfo, enlistmentId});
});

router.get('/grading', (req, res) => {
    res.render('admin/pages/grading',{title:'Grading'});
});

router.get('/reports', (req, res) => {
    res.render('admin/pages/reports',{title:'Reports'});
});

router.get('/forms', async (req, res) => {
    const forms = await formController.getAllForms()
    res.render('admin/pages/forms',{title:'Forms', forms:forms});
});





module.exports = router;