var express = require('express');
var router = express.Router();



const authenticationController = require('../controllers').authentication
/* GET home page. */
router.get('/', function(req, res) {
  res.render('login', { title: 'Login' });
});


router.post('/login', authenticationController.login);

router.get('/logout', authenticationController.logout);


module.exports = router;
