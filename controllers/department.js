const Department = require('../models').Department;
const DepartmentChairman = require('../models').DepartmentChairman;
const College = require('../models').College;
const Faculty = require('../models').Faculty;


async function listAll(req, res){
    const faculties = await Faculty.findAll();
    const colleges = await College.findAll();
    
    return Department
        .findAll({
            include: [
                {model: DepartmentChairman, as: 'departmentchairman',
                    include: [{model: Faculty, as: 'department', attributes: ['firstName', 'lastName']},]
                },
                {model: College, as: 'college'}
            ]
        })
        .then(dept => {
            // res.send({
            //     departments: dept
            // })

            return res.render('admin/pages/departments', { 
                department: dept,
                college: colleges,
                faculty: faculties,
                title: "Colleges"
              });
        })
        .catch(err => {
            // res.send({
            //     error: err
            // })
            console.log(err)
        });
}//end

async function getAll(){    
    return Department
        .findAll({
            include: [
                {model: DepartmentChairman, as: 'departmentchairman',
                    include: [{model: Faculty, as: 'department', attributes: ['firstName', 'lastName']},]
                },
                {model: College, as: 'college'}
            ]
        })
}//end

async function fetchDepartment(req, res){
    return Department
        .findOne(req.body.id, {
            include: [
                {model: DepartmentChairman, as: 'departmentchairman',
                    include: [{model: Faculty, as: 'department', attributes: ['firstName', 'lastName']},]
                },
                {model: College, as: 'college'}
            ]
        })
        .then(department => {
            res.json({
                fetch: department
            })
        })
        .catch(err => {
            res.json({
                error: err
            })
        });
}//end

async function updateDepartment(req, res){
    return Department
        .findOne({
            where: {
                departmentCode: req.params.departmentCode
            }
        }).then(department =>{
            if(!department){
                res.status(400).send({
                    err: 'department not found'
                })
            }
            return department.update({
                departmentCode: req.body.departmentCode,
                departmentDescription: req.body.departmentDescription,
                collegeCode: req.body.collegeCode
            }).then(patch => {
                res.send({
                    patch
                })
            }).catch(err => {
                res.send({err})
            })
        }).catch(err => {
            res.send({
                error: err
            })
        })    
}//end

async function createDepartment(req, res){ 
    fetch = Department.findOne({
        where: {
            departmentCode: req.body.departmentCode
        }
    }).then(async department => {
        if(department){
            return res.status(400).json({
                error: 'department code already exists'
            })
        }
        await DepartmentChairman.create({
            departmentCode: req.body.departmentCode,
            chairmane: req.body.chairman
        })
        return Department
        .create({
            departmentCode: req.body.departmentCode,
            departmentDescription: req.body.departmentDescription,
            collegeCode: req.body.collegeCode,
            active: req.body.active
        }).then(department => {
            res.json({
                created: department
            });
        })
        .catch(err => {
            res.json(err)
        });;
    }).catch(err => {
        res.json({
            error: err
        })
    });
}//end

async function activateDepartment(req, res){
   // console.log(req.body.departmentCode)
    return Department
        .findOne({
            where: {
                departmentCode: req.params.departmentCode
            }
        }).then(department =>{
            if(!department){
                return res.status(400).send({
                    err: 'department not found'
                })
            }
            if(department.active){
                return res.status(400).send({
                    err: 'department already active'
                })
            }
            return department.update({
                active: true
            }).then(patch => {
                res.send({
                    patch
                })
            }).catch(err => {
                res.send({err})
            })
        }).catch(err => {
            res.send({
                error: err
            })
        });
}//end

async function deactivateDepartment(req, res){
    return Department
        .findOne({
            where: {
                departmentCode: req.params.departmentCode
            }
        }).then(department =>{
            if(!department){
               return res.status(400).send({
                    err: 'department not found'
                })
            }
            if(!department.active){
               return res.status(400).send({
                    err: 'department already inactive'
                })
            }
            return department.update({
                active: false
            }).then(patch => {
                res.send({
                    patch
                })
            }).catch(err => {
                res.send({err})
            })
        }).catch(err => {
            res.send({
                error: err
            })
        });
}//end


module.exports = {
    listAll,
    createDepartment,
    updateDepartment,
    fetchDepartment,
    activateDepartment,
    deactivateDepartment,
    getAll
}
