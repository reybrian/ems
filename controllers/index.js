const studentInformation = require('./studentInformation');
const facultyInformation = require('./facultyInformation')
const staffInformation = require('./staffInformation')
const authentication = require('./authentication')
const accountsManagement = require('./accountsManagement')
const form = require('./forms')
const calendar = require('./calendar')
module.exports = {
  studentInformation,
  facultyInformation,
  staffInformation,
  authentication,
  accountsManagement,
  form,
  calendar
};