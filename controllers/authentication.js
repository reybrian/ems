const Account = require('../models').Account
const bcrypt = require('bcrypt');

module.exports = {

   login(req, res) {
      return Account.findOne({
         where: {
            schoolEmail: req.body.schoolEmail
         }
      }).then((account) => {

         if (!account) {
            return res.status(400).json('Email not found')
         }
         return {
            match: bcrypt.compareSync(req.body.password, account.password),
            account: account
         }
      }).then((data) => {
         if (!data.match) {
            return res.status(400).json('Incorrect password');
         }

         if(!data.account.isActive){
            return res.status(401).json('Account inactive');
         }
         
         req.session.user = data.account
         return res.status(200).json({
            message: 'Successful login',
            role: data.account.roleID
         })
      })
   },

   logout(req, res, next) {
      if (req.session) {
         req.session.destroy(function (err) {
            if (err) {
               return next(err);
            } else {
               return res.redirect('/');
            }
         });
      }
   },



}