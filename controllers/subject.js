const Subject = require('../models').Subject;

async function listAll(req, res){
    return Subject
        .findAll()
        // .then(subjects => {
        //     res.json({
        //         subjects
        //     })
        // })
        // .catch(err => {
        //     res.json({
        //         error: err
        //     })
        // });
}//end

async function fetchSubject(req, res){
    return Subject
        .findOne({where: {subjectCode: req.params.subjectCode}})
        .then(subject => {
            res.json({
                subject
            })
        })
        .catch(err => {
            res.json({
                error: 'subject not found'
            })
        });
}//end

async function updateSubject(req, res){
    console.log(req.body)
    return Subject
        .findOne({
            where: {
                subjectCode: req.params.subjectCode
            }
        }).then(subject =>{
            if(!subject){
                return res.status(400).send({
                    err: 'subject not found'
                })
            }
            return subject.update({
                subjectCode: req.body.subjectCode,
                isGPA: req.body.isGPA,
                subjectDescription: req.body.subjectDescription,
                totalWeeks: req.body.totalWeeks,
                totalHours: req.body.totalHours,
                lectureUnits: req.body.lectureUnits,
                labUnits: req.body.labUnits,
                totalUnits: req.body.totalUnits,
                labFee: req.body.labFee,
                facultyCredit: req.body.facultyCredit 
            }).then(patch => {
                res.send({
                    patch
                })
            }).catch(err => {
                res.send({err})
            })
        }).catch(err => {
            res.send({
                error: err
            })
        })    
}//end

async function createSubject(req, res){ 
    fetch = Subject.findOne({
        where: {
            subjectCode: req.body.subjectCode
        }
    }).then(subject => {
        if(subject){
            return res.status(400).send({
                error: 'subject code already exists'
            })
        }
        return Subject
        .create({
            subjectCode: req.body.subjectCode,
            isGPA: req.body.isGPA,
            subjectDescription: req.body.subjectDescription,
            totalWeeks: req.body.totalWeeks,
            totalHours: req.body.totalHours,
            lectureUnits: req.body.lectureUnits,
            labUnits: req.body.labUnits,
            totalUnits: req.body.totalUnits,
            labFee: req.body.labUnits,
            facultyCredit: req.body.facultyCredit
        }).then(subject => {
            res.send({subject});
        })
        .catch(err => {
            res.send(err)
        });;
    }).catch(err => {
        res.send({
            error: err
        })
    });
}//end


module.exports = {
    listAll,
    createSubject,
    fetchSubject,
    updateSubject
}