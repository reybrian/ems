const Clerk = require('../models').Clerk;
const Account = require('../models').Account
module.exports = {

    postClerk(req,res){

                return Clerk.findByPk(req.body.idNumber)
                .then((clerk) => {
                    if(clerk){
                        return res.status(409).send('ID number already exists')
                    }
                    return Clerk
                    .create({...req.body})
                        .then((clerk) => {
                            return res.status(201).json({posted:clerk})
                        })
                        .catch((error) => {
                            console.log(error)
                            return res.status(400).send(error)
                        })  
                })
                .catch((error) => {
                    console.log(error)
                    return res.status(400).send(error)
                }) 

    },

    getClerkByIdNumber(req,res){
        return Clerk
            .findByPk(req.params.idNumber , {
                include: [{
                    model: Account,
                    as: 'account'
                }]
            })
                .then((clerk) => {
                    if(!clerk){
                        return res.status(404).send({message: 'Clerk not found'})
                    }
                    return res.status(200).json({clerk:clerk})
                })
                .catch((error) => {
                    return res.status(400).send(error)
                })

    },

    getClerkByIdNumberReturn(id){
        return Clerk
            .findOne({where:{idNumber:id}})
                .then((clerk) => {
                    if(!clerk){
                        return 'Clerk not found'
                    }
                    return clerk
                })

    },

    getAllClerk(req,res){
        return Clerk
            .findAll({
                attributes: ['idNumber','firstName','lastName','middleName','clerkType'],
                order:  ['updatedAt'],
                include: [{
                    model: Account,
                    as: 'account'
                }]
            })
                // .then((clerks) => {return res.status(200).json({clerks:clerks})})
                // .catch((error) => {return res.status(400).send(error)})
    },

    getAllClerkStaff(req,res){
        return Clerk
            .findAll({
                attributes: ['idNumber','firstName','lastName','middleName','clerkType'],
                where: {
                    clerkType: '1'
                }
            })
                // .then((clerks) => {return res.status(200).json({clerks:clerks})})
                // .catch((error) => {return res.status(400).send(error)})
    },

    getAllClerkDepartmentClerk(req,res){
        return Clerk
            .findAll({
                attributes: ['idNumber','firstName','lastName','middleName','clerkType'],
                where: {
                    clerkType: '2'
                }
            })
                // .then((clerks) => {return res.status(200).json({clerks:clerks})})
                // .catch((error) => {return res.status(400).send(error)})
    },


    putClerk(req,res){
        return Clerk
            .update({...req.body},{where: {idNumber: req.params.idNumber}})
                .then((clerk) => {
                    if(!clerk[0]){
                        return res.status(400).send({message: 'Update Failed'})
                    }
                    return res.status(200).json({clerk:clerk})
                })
                .catch((error) => {
                    return res.status(400).send(error)
                })

    }

}