const Faculty = require('../models').Faculty
const Account = require('../models').Account
module.exports = {
    postFaculty(req,res){

            return Faculty.findByPk(req.body.idNumber)
            .then((faculty) => {
                if(faculty){
                    return res.status(409).send('ID number already exists')
                }
                return Faculty
                .create({...req.body})
                    .then((faculty) => {
                        return res.status(201).json({posted:faculty})
                    })
                    .catch((error) => {
                        console.log(error)
                        return res.status(400).send(error)
                    })  
            })
            .catch((error) => {
                console.log(error)
                return res.status(400).send(error)
            })  
    },

    getFacultyByIdNumber(req,res){
        return Faculty
            .findByPk(req.params.idNumber,{
                include: [{
                    model: Account,
                    as: 'account'
                }]
            })
                .then((faculty) => {
                    if(!faculty){
                        return res.status(404).send({message: 'Faculty not found'})
                    }
                    return res.status(200).json({faculty:faculty})
                })
                .catch((error) => {
                    return res.status(400).send(error)
                })

    },

    getAllFaculty(req,res){
        return Faculty
            .findAll({
                attributes: ['idNumber','firstName','lastName','middleName','departmentCode'],
                order:  ['updatedAt'],
                include: [{
                    model: Account,
                    as: 'account'
                }]
            })
                // .then((faculties) => {return res.status(200).json({faculties:faculties})})
                // .catch((error) => {return res.status(400).send(error)})
    },

    getAll(){
        return Faculty
            .findAll({
                attributes: ['idNumber','firstName','lastName','middleName','departmentCode']
            })
    },


    putFaculty(req,res){
        return Faculty
            .update({...req.body},{where: {idNumber: req.params.idNumber}})
                .then((faculty) => {
                    if(!faculty[0]){
                        return res.status(400).send({message: 'Update Failed'})
                    }
                    return res.status(200).json({faculty:faculty})
                })
                .catch((error) => {
                    return res.status(400).send(error)
                })

    }
    
}