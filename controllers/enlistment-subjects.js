const EnlistmentSubject = require('../models').EnlistmentSubjects;
const SubjectOffering = require('../models').SubjectOffering;
const SchoolYear = require('../models').SchoolYear;
const Block = require('../models').Block;
const Enlistment = require('../models').Enlistment;
const Student = require('../models').Student;
const Subject  = require('../models').Subject;
const Classroom = require('../models').Classroom;


async function getCurrentSemester(){
    return SchoolYear
        .findOne({
            where: {
                current: true
            }
        })
}

async function addEnlistmentSubject(req, res){
    currentSem = await getCurrentSemester();

    current = currentSem.id
    enlistment = req.body.enlistment;
    so = req.body.subjectOffering;

    // return res.json({
    //     current, enlistment, so
    // })
    return EnlistmentSubject
        .create({
            enlistment: req.body.enlistment,
            schoolYear: currentSem.id,
            subjectOffering: req.body.subjectOffering,
        })
        .then(e => {
           //return res.json({subject: e})
           return res.redirect('/admin/enlistment');
        })
        .catch(err => {
            return res.status(400).json(err);
        }) 
}

async function removeSubjectEnlistment(req, res){
    return EnlistmentSubject
        .findByPk(req.body.subjectEnlistment)
        .then(es => {
             es.destroy()
            .then(es => {
                return res.json({subject: es})
            }).catch(err => {
                return res.status(400).send(err);
            }) 
            
    }).catch(err => {
        return res.status(400).send(err);
    }) 
}

async function getStudentSubjects(sy, id){
    
    return EnlistmentSubject
        .findAll({
            include: [
                {model: Enlistment, as: 'studentenlistment', include: [{model: Student, as: 'student', attributes: ['idNumber', 'firstName', 'lastName', 'middleName']}]},
                {model: SubjectOffering, as: 'subjectoffering', include: [{model: Subject, as: 'subject'}, {model: Classroom, as: 'subjectclassroom'}]}
            ],
            where: {
                enlistment: id,
                schoolYear: sy
            },
        })
        .then(es => {
            return {subject: es}
        }).catch(err => {
            throw new Error(err)
        }) 
}

async function getStudentSubjectsApi(req, res){
    console.log("req.params")
    console.log(req.params)
    return EnlistmentSubject
        .findAll({
            include: [
                {model: Enlistment, as: 'studentenlistment', include: [{model: Student, as: 'student', attributes: ['idNumber', 'firstName', 'lastName', 'middleName']}]},
                {model: SubjectOffering, as: 'subjectoffering',  include: [{model: Subject, as: 'subject'}, {model: Classroom, as: 'subjectclassroom'}]}
            ],
            where: {
                enlistment: req.params.enlistment,
                schoolYear: req.params.schoolYear
            },
        })
        .then(es => {
            return res.json({subject: es});
        }).catch(err => {
            throw new Error(err);
        }) 
}


module.exports = {
    addEnlistmentSubject,
    removeSubjectEnlistment,
    getStudentSubjects,
    getStudentSubjectsApi
}