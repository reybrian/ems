const Classroom = require('../models').Classroom;
const College = require('../models').College;

async function listAll(){
    return Classroom
        .findAll({
            include: [{
                model: College, as: 'college'
            }]
        })
        
}//end

async function fetchRoom(req, res){
    return Classroom
        .findOne({where: {roomCode: req.params.roomCode}})
        .then(room => {
            if(!room){
                return res.status(400).send({
                    error: 'room not found'
                })
            }
            res.send({
                fetch: room
            })
        })
        .catch(err => {
            res.send({
                error: err
            })
        });
}//end

async function updateRoom(req, res){
    return Classroom
        .findOne({
            where: {
                roomCode: req.params.roomCode
            }
        }).then(room =>{
            if(!room){
                res.status(400).send({
                    err: 'classroom not found'
                })
            }
            return room.update({
                roomCode: req.body.roomCode,
                building: req.body.building,
                collegeCode: req.body.collegeCode
            }).then(patch => {
                res.send({
                    patch
                })
            }).catch(err => {
                res.send({err})
            })
        }).catch(err => {
            res.send({
                error: err
            })
        })    
}//end

async function createRoom(req, res){ 
    //console.log(req.body)
    fetch = Classroom.findOne({
        where: {
            roomCode: req.body.roomCode
        }
    }).then(room => {
        if(room){
            return res.status(400).send({
                error: 'room code already exists'
            })
        }
        return Classroom
        .create({
            roomCode: req.body.roomCode,
            building: req.body.building,
            collegeCode: req.body.collegeCode,
            active: req.body.active
        }).then(room => {
            res.send(room);
        })
        .catch(err => {
            res.send(err)
        });;
    }).catch(err => {
        res.send({
            error: err
        })
    });
}//end

async function activateRoom(req, res){
    return Classroom
        .findOne({
            where: {
                roomCode: req.params.roomCode
            }
        }).then(room =>{
            if(!room){
                return res.status(400).send({
                    err: 'room not found'
                })
            }
            if(room.active){
                return res.status(400).send({
                    err: 'room already active'
                })
            }
            return room.update({
                active: true
            }).then(patch => {
                res.send({
                    patch
                })
            }).catch(err => {
                res.send({err})
            })
        }).catch(err => {
            res.send({
                error: err
            })
        }) 
}//end

async function deactivateRoom(req, res){
    return Classroom
        .findOne({
            where: {
                roomCode: req.params.roomCode
            }
        }).then(room =>{
            if(!room){
                return res.status(400).send({
                    err: 'room not found'
                })
            }
            if(!room.active){
                return res.status(400).send({
                    err: 'room already inactive'
                })
            }
            return room.update({
                active: false
            }).then(patch => {
                res.send({
                    patch
                })
            }).catch(err => {
                res.send({err})
            })
        }).catch(err => {
            res.send({
                error: err
            })
        }) 
}//end



module.exports = {
    listAll,
    createRoom,
    updateRoom,
    fetchRoom,
    activateRoom,
    deactivateRoom
}