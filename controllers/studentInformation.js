const Student = require('../models').Student
const Account = require('../models').Account

module.exports = {

    postStudent(req,res){
        
       return Student.findByPk(req.body.idNumber)
        .then((student) => {
            if(student){
                return res.status(409).send('ID number already exists')
            }
            return Student
            .create({...req.body})
                .then((student) => {
                    return res.status(201).json({posted:student})
                })
                .catch((error) => {
                    console.log(error)
                    return res.status(400).send(error)
                })  
        })
        .catch((error) => {
            console.log(error)
            return res.status(400).send(error)
        })  
     

    },

    getStudentByIdNumber(req,res){
        return Student
            .findByPk(req.params.idNumber,{
                include: [{
                    model: Account,
                    as: 'account'
                }]
            })
                .then((student) => {
                    if(!student){
                        return res.status(404).send({message: 'Student not found'})
                    }
                    return res.status(200).json({student:student})
                })
                .catch((error) => {
                    console.log(error)
                    return res.status(400).send(error)
                })

    },

    fetchStudentByIdNumber(sid){
        return Student
            .findByPk(sid, {attributes: ['idNumber', 'firstName', 'middleName', 'lastName', 'suffix']})
    },

    getAllStudent(req,res){
        return Student
            .findAll({
                attributes: ['idNumber','firstName','lastName','middleName', 'updatedAt'],
                order:  ['updatedAt'],
                include: [{
                    model: Account,
                    as: 'account'
                }]
            })
                // .then((students) => {return res.status(200).json({students:students})})
                // .catch((error) => {return res.status(400).send(error)})
    },

    async listAll(){
        return Student
            .findAll({
                attributes: ['idNumber','firstName','lastName','middleName']
            })
    },
    getStudentByIdNumberReturn(id){
        return Student
            .findOne({where:{idNumber:id}})
                .then((student) => {
                    if(!student){
                        return 'Student not found'
                    }
                    return student
                })

    },

    putStudent(req,res){
        return Student
            .update({...req.body},{where: {idNumber: req.params.idNumber}})
                .then((student) => {
                    if(!student[0]){
                        return res.status(400).send({message: 'Update Failed'})
                    }
                    return res.status(200).json({student:student})
                })
                .catch((error) => {
                    return res.status(400).send(error)
                })

    }

}