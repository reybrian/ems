const College = require('../models').College;
const CollegeDean = require('../models').CollegeDean;
const Department = require('../models').Department;
const Faculty = require('../models').Faculty;

async function listAll(req, res){
    const faculties = await Faculty.findAll();
        return College
            .findAll({
                include: [
                    {model: CollegeDean, as: 'colldean',
                        include: [
                            {model: Faculty, as: 'collegedean', attributes: ['firstName', 'lastName']},
                            {model: Faculty, as: 'collegeassistantdean',  attributes: ['firstName', 'lastName']}
                        ]
                    },
                    {model: Department, as: 'departments'}
                ]
            })
            .then(college => {
              
                return res.render('admin/pages/colleges', { 
                    colleges: college,
                    faculty: faculties,
                    title: "Colleges"
                  });
            })
            .catch(err => {
               return res.send({
                    error: err
                })
            });
    }//end

    async function fetchCollege(req, res){
        return College
            .findByPk(
                req.params.id,
                {include: [
                    {model: CollegeDean, as: 'colldean',
                        include: [
                            {model: Faculty, as: 'collegedean', attributes: ['idNumber', 'firstName', 'lastName']},
                            {model: Faculty, as: 'collegeassistantdean',  attributes: ['idNumber', 'firstName', 'lastName']}
                        ]
                    },
                    {model: Department, as: 'departments'}
                ]}
            )
            .then(college => {
                res.json({
                    college
                })
            })
            .catch(err => {
                res.json({
                    err
                })
            });
    }//end

    async function updateCollege(req, res){
        
        coll = await College.update(
                {
                    collegeCode: req.body.collegeCode,
                    collegeDescription: req.body.collegeDescription 
                },
                {
                    where: {
                        collegeCode: req.body.old_collcode
                    }
                })
        colldean = await CollegeDean.update(
            {
                collegeCode: req.body.collegeCode,
                dean: req.body.dean,
                assistantDean: req.body.assistantDean
            },
            {
                where:{
                    collegeCode: req.body.old_collcode
                }
            }
        );

        return res.json({
            coll, colldean
        })
             
    }//end

    async function createCollege(req, res){ 
        //console.log(req.body)
        fetch = College.findOne({
            where: {
                collegeCode: req.body.collegeCode
            }
        }).then(async college => {
            if(college){
               return await updateCollege(req, res)
                .then(patch => {res.send({patch})})
                .catch(err => {res.send({err})});
            }

            const makeCollege = await College.create({
                collegeCode: req.body.collegeCode,
                collegeDescription: req.body.collegeDescription,
                active: req.body.active
            })
            const makeCollegeDean = await CollegeDean.create({
                collegeCode: req.body.collegeCode,
                dean: req.body.dean,
                assistantDean: req.body.assistantDean
            });

            res.send({makeCollege, makeCollegeDean});
            
        }).catch(err => {
            res.send({
                error: err
            })
        });
    }//end

    async function activateCollege(req, res){
        return College
            .findOne({
                where: {
                    collegeCode: req.params.collegeCode
                }
            }).then(college =>{
                if(!college){
                    return res.status(400).send({
                        err: 'college not found'
                    })
                }
                if(college.active){
                    return res.status(400).send({
                        err: 'college already active'
                    })
                }
                return college.update({
                    active: true
                }).then(patch => {
                    res.send({
                        patch
                    })
                }).catch(err => {
                    res.send({err})
                })
            }).catch(err => {
                res.send({
                    error: err
                })
            }) 
    }//end

    async function deactivateCollege(req, res){
        return College
        .findOne({
            where: {
                collegeCode: req.params.collegeCode
            }
        }).then(college =>{
            if(!college){
                return res.status(400).send({
                    err: 'college not found'
                })
            }
            if(!college.active){
                return res.status(400).send({
                    err: 'college already inactive'
                })
            }
            return college.update({
                active: false
            }).then(patch => {
                res.send({
                    patch
                })
            }).catch(err => {
                res.send({err})
            })
        }).catch(err => {
            res.send({
                error: err
            })
        }) 
    }//end

module.exports = {
    listAll,
    fetchCollege,
    updateCollege,
    createCollege,
    activateCollege,
    deactivateCollege
}