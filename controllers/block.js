const Block = require('../models').Block;
const College = require('../models').College;
const Department = require('../models').Department;
const SubjectOffering = require('../models').SubjectOffering;


async function listAll(){
    return Block
        .findAll({
            include: [
                {model: Department, as: 'department'},
                {model: College, as: 'college'}
            ]
        })
        // .then(blocks => {
        //     res.send({
        //         blocks
        //     })
        // })
        // .catch(err => {
        //     res.send({
        //         error: err
        //     })
        // });
}//end

async function fetchBlock(req, res){
    return Block
        .findOne({where: {blockCode: req.params.blockCode}})
        .then(block => {
            res.json({
                fetch: block
            })
        })
        .catch(err => {
            res.json({
                error: err
            })
        });
}//end

async function updateBlock(req, res){
    return Block
        .findByPk(req.params.id)
        .then(block =>{
            if(!block){
                res.status(400).send({
                    err: 'block not found'
                })
            }
            return block.update({
                blockCode: req.body.blockCode,
                forDepartment: req.body.forDepartment,
                forCollege: req.body.forCollege,
                active: req.body.active
            }).then(patch => {
                res.send({
                    patch
                })
            }).catch(err => {
                res.send({err})
            })
        }).catch(err => {
            res.send({
                error: err
            })
        })    
}//end

async function createBlock(req, res){ 
    fetch = Block.findOne({
        where: {
            blockCode: req.body.blockCode
        }
    }).then(block => {
        if(block){
            return res.status(400).send({
                error: 'block already exists'
            })
        }
        return Block
        .create({
            blockCode: req.body.blockCode,
            forDepartment: req.body.forDepartment,
            forCollege: req.body.forCollege,
            active: req.body.active
        }).then(block => {
            res.send({
                created: block
            });
        })
        .catch(err => {
            res.send(err)
        });;
    }).catch(err => {
        res.send({
            error: err
        })
    });
}//end


module.exports = {
    listAll,
    createBlock,
    fetchBlock,
    updateBlock
}