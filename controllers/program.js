const Program = require('../models').Program;
const ProgramMajor = require('../models').ProgramMajor;
const _ProgramMajor = require('./program-major');

async function listAll(){
    return Program
        .findAll({
            include: [{
                model: ProgramMajor, as: 'majors'
            }]
        })
        // .then(programs => {
        //     res.send({
        //         programs
        //     })
        // }).catch(err => {
        //     res.send({
        //         error: err
        //     })
        // })
}

async function fetchProgram(req, res){
    return Program
        .findOne({
            where: {programCode: req.params.programCode},
            include: [{
                model: ProgramMajor, as: 'majors'
            }]
        })
        .then(program => {
            if(!program){
                return res.status(400).send({
                    error: 'program not found'
                })
            }
            res.send({
                program
            })
        })
        .catch(err => {
            res.send({
                error: err
            })
        });
}//end

async function updateProgram(req, res){
    console.log(req.body)
    return Program
        .findOne({
            where: {
                programCode: req.params.programCode
            }
        }).then(async program => {
            if(!program){
                res.status(400).send({
                    err: 'program not found'
                })
            }

            patched = await program.update({
                programCode: req.body.programCode,
                programDesc: req.body.programDesc,
                programType: req.body.programType,
                deliveringDepartment: req.body.deliveringDepartment,
                totalSemester: req.body.totalSemester,
                totalUnits: req.body.totalUnits,
                active: req.body.active
            });

            majorInfo = {
                programCode: req.params.programCode,
                majorDescription: ""
            };
            patchMajor = req.body.majors;
            majors = await _ProgramMajor.getProgramMajors(majorInfo);

            //adds major if not already in db
            for(var i = 0; i < patchMajor.length; i++){
                majorInfo['majorDescription'] = patchMajor[i];
                if(!majors.includes(patchMajor[i])){
                    await addMajor(majorInfo);
                }
                if(majors.includes(patchMajor[i])){
                    await activateMajor(majorInfo);
                }
            }

            //deactivates major
            for(var i = 0; i < majors.length; i++){
                if(!patchMajor.includes(majors[i])){
                    majorInfo['majorDescription'] = majors[i];
                    await deactivateMajor(majorInfo);
                }
            }

            return fetchProgram(req, res)
            
        }).catch(err => {
            res.send({
                error: err
            })
        })    
}//end

async function createProgram(req, res){ 
    fetch = Program.findOne({
        where: {
            programCode: req.body.programCode
        }
    }).then(async program => {

        if(program){
            return res.status(400).send({
                error: 'program code already exists'
            })
        }
        created = await Program.create({
            programCode: req.body.programCode,
            programDesc: req.body.programDesc,
            programType: req.body.programType,
            deliveringDepartment: req.body.deliveringDepartment,
            totalSemester: req.body.totalSemester,
            totalUnits: req.body.totalUnits,
            active: req.body.active
        });

        if(req.body.majors){
            for(var i = 0; i < req.body.majors.length; i++){
                majorInfo = {
                    majorDescription: req.body.majors[i],
                    forProgram: req.body.programCode
                };
                addMajor(majorInfo);
            }
        }

        return res.send({created});

    }).catch(err => {
        res.send({
            error: err
        })
    });
}//end

async function activateProgram(req, res){
    return Program
        .findOne({
            where: {
                programCode: req.params.programCode
            }
        }).then(program =>{
            if(!program){
                return res.status(400).send({
                    err: 'program not found'
                })
            }
            if(program.active){
                return res.status(400).send({
                    err: 'program already active'
                })
            }
            return program.update({
                active: true
            }).then(patch => {
                res.send({
                    patch
                })
            }).catch(err => {
                res.send({err})
            })
        }).catch(err => {
            res.send({
                error: err
            })
        }) 
}//end

async function deactivateProgram(req, res){
    return Program
        .findOne({
            where: {
                programCode: req.params.programCode
            }
        }).then(program =>{
            if(!program){
                return res.status(400).send({
                    err: 'program not found'
                })
            }
            if(!program.active){
                return res.status(400).send({
                    err: 'program already inactive'
                })
            }
            return program.update({
                active: false
            }).then(patch => {
                res.send({
                    patch
                })
            }).catch(err => {
                res.send({err})
            })
        }).catch(err => {
            res.send({
                error: err
            })
        }) 
}//end

async function addMajor(majorInfo){
    return await _ProgramMajor.createMajor(majorInfo)
}

async function deactivateMajor(majorInfo){
    return await _ProgramMajor.deactivateMajor(majorInfo);
}

async function activateMajor(majorInfo){
    return await _ProgramMajor.activateMajor(majorInfo);
}

module.exports = {
    listAll,
    createProgram,
    fetchProgram,
    updateProgram,
    activateProgram,
    deactivateProgram
}