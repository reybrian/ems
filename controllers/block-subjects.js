const BlockSubject = require('../models').BlockSubjects;
const SchoolYear = require('../models').SchoolYear;
const Subject = require('../models').Subjects;
const SubjectOffering = require('../models').SubjectOffering;

async function getCurrentSemester(){
    return SchoolYear
        .findOne({
            where: {
                current: true
            }
        })
}

async function addBlockSubject(req, res){
    currentSem = await getCurrentSemester();

    return BlockSubject
        .create({
            blockCode: req.body.blockCode,
            schoolYear: currentSem.id,
            subjectOffering: req.body.subjectOffering
        }).then(bs => {
            res.send({bs});
        })
        .catch(err => {
            res.status(400).send(err)
        });
}

async function removeBlockSubject(req, res){
    return BlockSubject
        .findOne({
            where: {
                subjectOffering: req.body.subjectOffering,
                blockCode: req.body.blockCode
            }
        }).then(bs => {
            if(!bs){
                return res.send('not found');
            }
            return bs.destroy()
                .then(b => {
                    return res.send(b)
                }).catch(err => {res.send(err)})
        }).catch(err => {res.status(400).send(err)});
}

async function getBlockSubjects(req, res){
    currentSem = await getCurrentSemester();
    return BlockSubject
        .findAll({
            where: {
                blockCode: req.params.blockCode,
                schoolYear: currentSem.id
            },
            include: [
                {model: SubjectOffering, as: 'subject-offering'}
            ]
        }).then(bs => {
            return res.send({blockSubjects: bs})            
        }).catch(err => {res.send(err)});
}

module.exports = {
    addBlockSubject,
    removeBlockSubject,
    getBlockSubjects
}