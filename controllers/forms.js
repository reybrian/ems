const Form = require('../models').Form
module.exports = {

    postForm(req,res){

        req.body.filePath = req.file.path

            return Form
            .create({...req.body})
                .then((form) => {
                    return res.status(201).json({form:form})
                })
                .catch((error) => {
                    console.log(error)
                    return res.status(400).send(error)
                })  
        },

        putForm(req,res){
            if(req.file){
            req.body.filePath = req.file.path
            }
                return Form
                .update({...req.body}, {
                    where: {
                        id: req.params.id
                    }
                })
                    .then((form) => {
                        return res.status(201).json({form:form})
                    })
                    .catch((error) => {
                        console.log(error)
                        return res.status(400).send(error)
                    })  
            },

    getAllForms(req,res){
        return Form.findAll()
    },

    getFormById(req,res){
        console.log(typeof req.params.id)
        return Form
            .findByPk(req.params.id)
                .then((form) => {
                    if(!form){
                        return res.status(404).send({message: 'Form not found'})
                    }
                    return res.status(200).json({form:form})
                })
                .catch((error) => {
                    return res.status(400).send(error)
                })

    },

    deleteForm(req,res){
        return Form
            .findByPk(req.params.id)
                .then((form) => {
                    if (!form) {
                        return res.status(404).send({
                            message: "Form does not exist!"
                        });
                    }
                    return form
                        .destroy()
                        .then((form) => res.status(200).send('deleted'))
                        .catch((error) => res.status(400).send(error));
                })
                .catch((error) => {
                    return res.status(400).send(error)
                })

    },
        

}