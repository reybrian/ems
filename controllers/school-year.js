const db = require('../models/index');
const SchoolYear = require('../models').SchoolYear;


async function listAll(req, res){
    return SchoolYear
        .findAll()
        .then(sy => {
            res.send({sy})
        }).catch(err => {
            err
        })
}

async function createSchoolyear(req, res){
    await db.sequelize.query('UPDATE "SchoolYears" SET current = false');
    return SchoolYear
        .create({
            schoolYear: req.body.schoolYear,
            semester: req.body.semester,
            current: true
        }).then(sy => {
            res.send({
                sy
            })
        }).catch(err => {
            res.send({
                err
            })
        });
}

async function getCurrent(){
    return SchoolYear
        .findOne({
            where: {
                current: true
            }
        });
        // .then(sy => {
        //     res.send({
        //         current: sy
        //     })
        // }).catch(err => {
        //     res.send({
        //         err
        //     })
        // });
}

async function closeSemester(req, res){
    return SchoolYear
        .findOne({
            where: {
                current: true
            }
        }).then(sy => {
            return sy.update({
                current: false
            })
        }).catch(err => {
            res.send({
                err
            })
        });
}


module.exports = {
    createSchoolyear,
    listAll,
    getCurrent,
    closeSemester
}