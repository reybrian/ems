const SubjectOffering = require('../models').SubjectOffering;
const SchoolYear = require('../models').SchoolYear;
const Faculty = require('../models').Faculty;
const Subject = require('../models').Subject;
const Classroom = require('../models').Classroom;

async function getBoolean(a){
    if(a === 'on'){
        return true;
    }
    return false;
}

async function getCurrentSemester(){
    return SchoolYear
        .findOne({
            where: {
                current: true
            }
        })
}

async function createSubjectOffering(req, res){
    
    currentSem = await getCurrentSemester();

    if(!currentSem){
        return res.send({
            error: 'set current semester first'
        })
    }

    return SubjectOffering
        .create({
            schoolYear: currentSem.id,
            subjectCode: req.body.subjectCode,
            section: req.body.section,
            blockCode: req.body.blockCode,
            deliveringDepartment: req.body.deliveringDepartment,
            faculty: req.body.faculty,
            monday: await getBoolean(req.body.monday),
            tuesday: await getBoolean(req.body.tuesday),
            wednesday: await getBoolean(req.body.wednesday),
            thursday: await getBoolean(req.body.thursday),
            friday: await getBoolean(req.body.friday),
            startTime: req.body.startTime,
            endTime: req.body.endTime,
            slots: req.body.slots,
            classroom: req.body.classroom,
            fee: req.body.fee
        }).then(subjectOffering => {
            res.redirect('/admin/offering');
        })
        .catch(err => {
            res.status(400).send(err)
        });

}

async function listAll(req, res){
    return SubjectOffering
        .findAll({
            include: [
                {model: Faculty, as: 'subject-faculty', attributes:['firstName', 'middleName', 'lastName']},
                {model: Subject, as: 'subject'},
                {model: Classroom, as: 'subjectclassroom'}
            ]
        })
        .then(so => {
            res.send({
                subjectOfferings: so
            })
        })
        .catch(err => {
            res.send({
                error: err
            })
        });
}

async function getCurrentSubjectOfferings(){
    currentSem = await getCurrentSemester();
    return SubjectOffering
    .findAll(
        {include: [
            {model: Faculty, as: 'subject-faculty', attributes:['firstName', 'middleName', 'lastName']},
            {model: Subject, as: 'subject'},
            {model: Classroom, as: 'subjectclassroom'}
        ]},
        {
            where: {
                schoolYear: currentSem.id,
            }
        }
        )
    //     .then(so => {
    //     res.send({
    //         subjectOfferings: so
    //     })
    // })
    // .catch(err => {
    //     res.send({
    //         error: err
    //     })
    // });
}

async function fetchOffering(req, res){
    return SubjectOffering
        .findByPk(req.params.id,
            {
                include: [
                    {model: Faculty, as: 'subject-faculty', attributes:['firstName', 'middleName', 'lastName']},
                    {model: Subject, as: 'subject'}
                ]
            }
        )
        .then(subjectOffering => {
            res.send({
                subjectOffering
            })
        })
        .catch(err => {
            res.send({
                error: err
            })
        })
}

async function updateOffering(req, res){
    return SubjectOffering
    .findOne({
        where: {
            id: req.params.id
        }
    }).then(so =>{
        if(!so){
            res.status(400).send({
                err: 'not found'
            })
        }
        return so.update({
            subjectCode: req.body.subjectCode,
            section: req.body.section,
            blockCode: req.body.blockCode,
            deliveringDepartment: req.body.deliveringDepartment,
            faculty: req.body.faculty,
            monday: req.body.monday,
            tuesday: req.body.tuesday,
            wednesday: req.body.wednesday,
            thursday: req.body.thursday,
            friday: req.body.friday,
            startTime: req.body.startTime,
            endTime: req.body.endTime,
            slots: req.body.slots,
            classroom: req.body.classroom,
            fee: req.body.fee
        }).then(patch => {
            res.send({
                patch
            })
        }).catch(err => {
            res.send({err})
        })
    }).catch(err => {
        res.send({
            error: err
        })
    })    
}


module.exports = {
    createSubjectOffering,
    fetchOffering,
    updateOffering,
    listAll,
    getCurrentSubjectOfferings
}

