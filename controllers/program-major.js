const ProgramMajor = require('../models').ProgramMajor;

async function createMajor(majorInfo){
    return ProgramMajor
        .create({
            majorDescription: majorInfo.majorDescription,
            forProgram: majorInfo.programCode,
            active: true
        });
}

async function deactivateMajor(majorInfo){
    return ProgramMajor
        .update(
            {
                active: false
            },
            {
                where: {
                    majorDescription: majorInfo.majorDescription,
                    forProgram: majorInfo.programCode
                }
            }
        );
}

async function activateMajor(majorInfo){
    return ProgramMajor
        .update(
            {
                active: true
            },
            {
                where: {
                    majorDescription: majorInfo.majorDescription,
                    forProgram: majorInfo.programCode
                }
            }
        );
}

async function getProgramMajors(req, res){
    majors = [];
    fetched = await ProgramMajor.findAll({ 
        where: {
            forProgram: req.params.programCode
        },
        attributes: ['majorDescription']
    });

    for(var i = 0; i< fetched.length; i++){
        majors.push(fetched[i].majorDescription);
    }

    return res.json({majors});
}

module.exports = {
    createMajor,
    deactivateMajor,
    getProgramMajors,
    activateMajor
}
