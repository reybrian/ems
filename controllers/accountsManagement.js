const Account = require('../models').Account
const Role = require('../models').Role
const Clerk = require('../models').Clerk
const Student = require('../models').Student
const Faculty = require('../models').Faculty

const bcrypt = require('bcrypt');
module.exports = {

   async postStudentAccount(req,res){
        
        req.body.password = await bcrypt.hashSync(req.body.password, 5)

        return Account
            .create({...req.body})
            .then((account) => {return res.status(201).json({account:account})})
            .catch((error) => res.status(400).send(error))
    },

    getAllAccounts(req,res){
        return Account
            .findAll({
                attributes: ['idNumber','schoolEmail','roleID', 'isActive'],
                include: [{
                    model: Role
                }]
            })
                // .then((faculties) => {return res.status(200).json({faculties:faculties})})
                // .catch((error) => {return res.status(400).send(error)})
    },

    putStatus(req,res){
        return Account
        .update({...req.body},{where: {idNumber: req.params.idNumber}})
            .then((account) => {
                if(!account[0]){
                    return res.status(400).send({message: 'Update Failed'})
                }
                return res.status(200).json({account:account})
            })
            .catch((error) => {
                return res.status(400).send(error)
            })
    },

    async resetPass(req,res){

        req.body.password = await bcrypt.hashSync(req.body.password, 5)

        return Account
        .update({...req.body},{where: {idNumber: req.params.idNumber}})
            .then((account) => {
                if(!account[0]){
                    return res.status(400).send({message: 'Update Failed'})
                }
                return res.status(200).json({account:account})
            })
            .catch((error) => {
                return res.status(400).send(error)
            })
    },


    getAccountbyIdNumber(req,res){

      
        return Account
        .findOne({
            where : {
                idNumber: req.params.idNumber
            },
            include: [{
                model: Clerk,
                as: 'clerkprofile'
            },{
                model: Student,
                as: 'studentprofile'
            },{
                model: Faculty,
                as: 'facultyprofile'
            },{
                model: Role,
            }]
        })
            .then((account) => {
                if(!account){
                    return res.status(404).send({message: 'Account not found'})
                }
                return res.status(200).json({account:account})
            })
            .catch((error) => {
                console.log(error)
                return res.status(400).send(error)
            })
    },
    
}