const Enlistment = require('../models').Enlistment;
const SchoolYear = require('../models').SchoolYear;
const Student = require('../models').Student;

async function getCurrentSemester(){
    return SchoolYear
        .findOne({
            where: {
                current: true
            }
        })
}

async function addEnlistment(req, res){
    currentSem = await getCurrentSemester();
    return Enlistment
        .create({
            schoolYear: currentSem.id,
            studentId: req.body.idNumber,
            yearLevel: req.body.yearLevel,
            programCode: req.body.programCode,
            majorCode: req.body.majorCode,
            graduating: (req.body.graduating === 'on') ? true : false
        })
        .then(e => {
           return res.redirect('/admin/enlistment');
        })
        .catch(err => {
            return res.status(400).send(err);
        })
}

async function updateEnlistment(req, res){
    return Enlistment
        .findByPk(req.params.id)
        .then(e => {
            if(!e){
                return res.status(400).send('not found');
            }
            return e.update({
                yearLevel: req.body.yearLevel,
                programCode: req.body.programCode,
                majorCode: req.body.major,
                graduating: req.body.graduating
            }).then(e => {
                return res.json({advised: e})
            })
            .catch(err => {
                return res.status(400).json(err);
            })
        }).then(e => {
            return res.json({advised: e})
        })
        .catch(err => {
            return res.status(400).json(err);
        })
}

async function listAll(req, res){
    return Enlistment
        .findAll(
            {
                include: [{model: Student, as: 'student', attributes: ['idNumber', 'firstName', 'lastName']}]
            }
        )
        .then(e => {
            res.send({enlistments: e})
        })
        .catch(err => {
            res.status(400).send(err)
        })
}

async function listAllCurrent(req, res){
    currentSem = await getCurrentSemester();

    return Enlistment
        .findAll( 
            {
                include: [{model: Student, as: 'student', attributes: ['idNumber', 'firstName', 'lastName', 'middleName']}],
                where: {
                    schoolYear: currentSem.id
                }
            }
        )
        // .then(e => {
        //     res.send({enlistments: e})
        // })
        // .catch(err => {
        //     res.status(400).send(err)
        // })
}

async function fetchEnlistment(req, res){
    return Enlistment
        .findByPk(req.params.id,{
            include: [{model: Student, as: 'student', attributes: ['idNumber', 'firstName', 'lastName', 'middleName']}]
        })
        .then(e => {
            return res.json({e})
        })
        .catch(err => {
            return res.status(400).json(err);
        })
}


module.exports = {
    addEnlistment,
    updateEnlistment,
    listAll,
    listAllCurrent,
    fetchEnlistment
}